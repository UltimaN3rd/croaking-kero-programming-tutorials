#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#define XK_MISCELLANY
#define XK_LATIN1
#include <X11/keysymdef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <X11/XKBlib.h>
#include <time.h>
typedef struct timespec timespec;

#define min(a, b) ((a)<(b) ? (a):(b))

Display* display;
int root_window;
int screen;
Window window;
Atom WM_DELETE_WINDOW;
uint8_t* canvas;
XImage* window_image;
GC graphics_context;
int window_width = 1280, window_height = 720;
bool keyboard[256] = {0};

int main(int argc, char* argv[]){
    
    // Create window
    XVisualInfo visual_info;
    {
        display = XOpenDisplay(0);
        root_window = DefaultRootWindow(display);
        screen = DefaultScreen(display);
        XMatchVisualInfo(display, screen, 24, TrueColor, &visual_info);
        XSetWindowAttributes window_attributes;
        window_attributes.background_pixel = 0;
        window_attributes.colormap = XCreateColormap(display, root_window, visual_info.visual, AllocNone);
        window_attributes.event_mask = StructureNotifyMask | KeyPressMask | KeyReleaseMask | FocusChangeMask;
        window = XCreateWindow(display, root_window, 0, 0, window_width, window_height, 0, visual_info.depth, 0, visual_info.visual, CWBackPixel | CWColormap | CWEventMask, &window_attributes);
        XMapWindow(display, window);
        XFlush(display);
        WM_DELETE_WINDOW = XInternAtom(display, "WM_DELETE_WINDOW", False);
        XSetWMProtocols(display, window, &WM_DELETE_WINDOW, 1);
        canvas = (uint8_t*)malloc(window_width*window_height*4);
        window_image = XCreateImage(display, visual_info.visual, visual_info.depth, ZPixmap, 0, (char*)canvas, window_width, window_height, 32, 0);
        graphics_context = DefaultGC(display, screen);
        XkbSetDetectableAutoRepeat(display, True, 0);
    }
    
    bool game_running = true;
    int x = 0, y = 0;
    float delta = 0;
    timespec frame_start;
    timespec frame_finish;
    clock_gettime(CLOCK_REALTIME, &frame_start);
    long unsigned target_frame_time = 1000000000/60; // In nano seconds
    
    // Main loop
    while(game_running){
        
        memset(canvas, 0, window_width*window_height*4);
        
        // Handle events
        XEvent e;
        while(XPending(display) > 0){
            XNextEvent(display, &e);
            switch(e.type){
                case KeyPress:{
                    int symbol = XLookupKeysym(&e.xkey, 0);
                    keyboard[(uint8_t)symbol] = true;
                    switch(symbol){
                        case XK_Escape:{
                            game_running = false;
                        }break;
                    }
                }break;
                case KeyRelease:{
                    int symbol = XLookupKeysym(&e.xkey, 0);
                    keyboard[(uint8_t)symbol] = false;
                }break;
                case ClientMessage:{
                    XClientMessageEvent* ev = (XClientMessageEvent*)&e;
                    if((Atom)ev->data.l[0] == WM_DELETE_WINDOW){
                        game_running = false;
                    }
                }break;
                case ConfigureNotify:{
                    XConfigureEvent* ev = (XConfigureEvent*)&e;
                    if(ev->width != window_width || ev->height != window_height){
                        window_width = ev->width;
                        window_height = ev->height;
                        XDestroyImage(window_image);
                        canvas = (uint8_t*)malloc(window_width*window_height*4);
                        window_image = XCreateImage(display, visual_info.visual, visual_info.depth, ZPixmap, 0, (char*)canvas, window_width, window_height, 32, 0);
                        x = min(x, window_width-1);
                        y = min(y, window_height-1);
                    }
                }break;
                case DestroyNotify:{
                    game_running = false;
                }break;
                case FocusOut:{
                    memset(keyboard, false, sizeof(keyboard));
                }break;
            }
        }
        
        // Move our pixel
        if(keyboard[(uint8_t)XK_Up]){
            if(--y < 0){
                y = 0;
            }
        }
        if(keyboard[(uint8_t)XK_Down]){
            if(++y >= window_height){
                y = window_height-1;
            }
        }
        if(keyboard[(uint8_t)XK_Left]){
            if(--x < 0){
                x = 0;
            }
        }
        if(keyboard[(uint8_t)XK_Right]){
            if(++x >= window_width){
                x = window_width-1;
            }
        }
        
        *((uint32_t*)(canvas) + y*window_width+x) = 0xffffffff;
        // Update window
        XPutImage(display, window, graphics_context, window_image, 0, 0, 0, 0, window_width, window_height);
        
        // Delay until we take up the full frame time
        clock_gettime(CLOCK_REALTIME, &frame_finish);
        long unsigned frame_time = (frame_finish.tv_sec - frame_start.tv_sec)*1000000000 + (frame_finish.tv_nsec - frame_start.tv_nsec);
        timespec sleep_time;
        sleep_time.tv_sec = 0;
        sleep_time.tv_nsec = target_frame_time - frame_time;
        nanosleep(&sleep_time, 0);
        clock_gettime(CLOCK_REALTIME, &frame_finish);
        delta = (frame_finish.tv_sec - frame_start.tv_sec) + (frame_finish.tv_nsec - frame_start.tv_nsec)/1000000000.f;
        frame_start = frame_finish;
        char window_title[30];
        sprintf(window_title, "Nick's Xlib window. FPS: %0.02f", 1.f/delta);
        XStoreName(display, window, window_title);
    }
    
    return 0;
}
