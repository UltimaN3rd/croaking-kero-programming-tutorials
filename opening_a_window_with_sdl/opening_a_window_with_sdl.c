#include <SDL2/SDL.h>
#include <stdint.h>
#include <stdbool.h>

#define min(a,b) ((a)<(b) ? (a):(b))

int main(int argc, char* argv[]){
    int window_width = 1280, window_height = 720;
    
    SDL_Init(0);
    SDL_Window* window = SDL_CreateWindow("Nick's SDL2 window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, SDL_WINDOW_RESIZABLE);
    SDL_Surface* canvas = SDL_GetWindowSurface(window);
    
    
    bool game_running = true;
    int x = 0, y = 0;
    const uint8_t* keyboard = SDL_GetKeyboardState(NULL);
    
    Uint64 ticks_per_second = SDL_GetPerformanceFrequency();
    Uint64 target_frame_time = ticks_per_second / 60; // 60 = target framerate
    Uint64 frame_start;
    Uint64 frame_finish;
    frame_start = SDL_GetPerformanceCounter();
    
    // Main loop
    while(game_running){
        memset(canvas->pixels, 0, window_width*window_height*4);
        
        SDL_Event e;
        while(SDL_PollEvent(&e)){
            switch(e.type){
                case SDL_KEYDOWN:{
                    if(!e.key.repeat){
                        switch(e.key.keysym.sym){
                            case SDLK_ESCAPE:{
                                game_running = false;
                            }break;
                        }
                    }break;
                }
                case SDL_WINDOWEVENT:{
                    switch(e.window.event){
                        case SDL_WINDOWEVENT_SIZE_CHANGED:{
                            window_width = e.window.data1;
                            window_height = e.window.data2;
                            // SDL_FreeSurface(canvas);
                            canvas = SDL_GetWindowSurface(window);
                            x = min(x, window_width-1);
                            y = min(y, window_height-1);
                        }break;
                    }
                }break;
                case SDL_QUIT:{
                    game_running = false;
                }break;
            }
        }
        
        if(keyboard[SDL_SCANCODE_UP]){
            if(--y < 0){
                y = 0;
            }
        }
        if(keyboard[SDL_SCANCODE_DOWN]){
            if(++y > window_height-1){
                y = window_height-1;
            }
        }
        if(keyboard[SDL_SCANCODE_LEFT]){
            if(--x < 0){
                x = 0;
            }
        }
        if(keyboard[SDL_SCANCODE_RIGHT]){
            if(++x > window_width-1){
                x = window_width-1;
            }
        }
        
        *((uint32_t*)(canvas->pixels) + y*window_width+x) = 0xffffffff;
        SDL_UpdateWindowSurface(window);
        
        frame_finish = SDL_GetPerformanceCounter();
        float sleep_time_in_ticks = (float)target_frame_time - (float)(frame_finish - frame_start);
        float sleep_time_in_ms = sleep_time_in_ticks/ticks_per_second*1000.f;
        if(sleep_time_in_ms > 0){
            SDL_Delay(sleep_time_in_ms);
        }
        
        frame_finish = SDL_GetPerformanceCounter();
        float delta = (float)(frame_finish - frame_start) / (float)ticks_per_second;
        frame_start = frame_finish;
        char title[30];
        sprintf(title, "Nick's SDL2 window. FPS: %0.02f", 1.f/delta);
        SDL_SetWindowTitle(window, title);
    }
    
    return 0;
}
