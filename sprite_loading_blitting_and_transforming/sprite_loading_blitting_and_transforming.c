#include "kero_window.h"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <stb/stb_image.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#define max(a, b) (a>b?a:b)
#define min(a, b) (a<b?a:b)

bool game_running;

typedef struct{
    int w, h;
    uint8_t* pixels;
} Sprite;

bool SpriteLoad(Sprite* sprite, char* filepath){
    sprite->pixels = stbi_load(filepath, &sprite->w, &sprite->h, 0, 4);
    //if(PNGLoad(filepath, &sprite->pixels, &sprite->w, &sprite->h)){
    if(!sprite->pixels){
        //printf("Failed to load image: %s. %s\n", filepath, stbi_failure_reason());
        printf("Failed to load image: %s\n", filepath);
        return false;
    }
    for(int y = 0; y < sprite->h; y++){
        for(int x = 0; x < sprite->w; x++){
            uint8_t* b = (sprite->pixels + y*sprite->w*4 + x*4);
            uint8_t* r = (sprite->pixels + y*sprite->w*4 + x*4 + 2);
            uint8_t blue_temp = *b;
            *b = *r;
            *r = blue_temp;
        }
    }
    return true;
}

uint32_t SpriteGetPixel(Sprite* sprite, int x, int y){
    if(x < 0 || x > sprite->w-1 || y < 0 || y > sprite->h-1)return 0;
    return *((uint32_t*)(sprite->pixels) + y*sprite->w + x);
}

uint32_t SpriteGetPixelf(Sprite* sprite, float x, float y){
    if(x < 0 || (int)x > sprite->w-1 || y < 0 || (int)y > sprite->h-1)return 0;
    return *((uint32_t*)(sprite->pixels) + (int)y*sprite->w + (int)x);
}

void SpritePutPixel(Sprite* target, int x, int y, uint32_t pixel){
    if(x < 0 || x > target->w-1 || y < 0 || y > target->h-1)return;
    *((uint32_t*)(target->pixels) + y*target->w + x) = pixel;
}

void SpritePutPixelAlphaBlended(Sprite* target, int x, int y, uint32_t pixel){
    if(x < 0 || x > target->w-1 || y < 0 || y > target->h-1)return;
    float source_alpha = (float)((uint8_t)(pixel>>24))/255.f;
    float target_alpha = 1.f-source_alpha;
    uint8_t* target_pixel_byte = target->pixels + (y*target->w + x)*4;
    *target_pixel_byte++ = ((uint8_t)pixel)*source_alpha + (*target_pixel_byte)*target_alpha;
    *target_pixel_byte++ = ((uint8_t)(pixel>>8))*source_alpha + (*target_pixel_byte)*target_alpha;
    *target_pixel_byte++ = ((uint8_t)(pixel>>16))*source_alpha + (*target_pixel_byte)*target_alpha;
}

void SpriteBlit(Sprite* sprite, Sprite* target, int x, int y){
    int left_clip = max(0, -x);
    int right_clip = max(0, x + sprite->w - target->w);
    if(left_clip + right_clip > sprite->w) return;
    int top_clip = max(0, -y);
    int bottom_clip = max(0, y + sprite->h - target->h);
    if(top_clip + bottom_clip > sprite->h) return;
    
    //memcpy(target->pixels + ((y+i)*target->w + x + left_clip)*4, sprite->pixels + (i*sprite->w + left_clip)*4, (sprite->w - left_clip - right_clip)*4);
    for(int sx = left_clip; sx < sprite->w - right_clip; sx++){
        for(int sy = top_clip; sy < sprite->h - bottom_clip; sy++){
            SpritePutPixelAlphaBlended(target, x+sx, y+sy, SpriteGetPixel(sprite, sx, sy));
        }
    }
}

void SpriteBlitScaledOrigin(Sprite* sprite, Sprite* target, int x, int y, int scale, int originx, int originy){
    int left = x - originx*scale;
    int top = y - originy*scale;
    
    for(int sy = 0; sy < sprite->h; sy++){
        for(int sx = 0; sx < sprite->w; sx++){
            uint32_t source_pixel = SpriteGetPixel(sprite, sx, sy);
            int scaled_sourcex = sx*scale;
            int scaled_sourcey = sy*scale;
            for(int oy = min(0,scale); oy < max(scale,0); oy++){
                for(int ox = min(0,scale); ox < max(scale,0); ox++){
                    SpritePutPixelAlphaBlended(target, left + scaled_sourcex + ox, top + scaled_sourcey + oy, source_pixel);
                }
            }
        }
    }
}
void SpriteBlitScaled(Sprite* sprite, Sprite* target, int x, int y, int scale){
    SpriteBlitScaledOrigin(sprite, target, x, y, scale, sprite->w/2, sprite->h/2);
}

void SpriteBlitScaledXY(Sprite* sprite, Sprite* target, int x, int y, int scalex, int scaley, int originx, int originy){
    int left = x - originx*scalex;
    int top = y - originy*scaley;
    
    for(int sy = 0; sy < sprite->h; sy++){
        for(int sx = 0; sx < sprite->w; sx++){
            uint32_t source_pixel = SpriteGetPixel(sprite, sx, sy);
            int scaled_sourcex = sx*scalex;
            int scaled_sourcey = sy*scaley;
            for(int oy = min(0,scaley); oy < max(scaley,0); oy++){
                for(int ox = min(0,scalex); ox < max(scalex,0); ox++){
                    SpritePutPixelAlphaBlended(target, left + scaled_sourcex + ox, top + scaled_sourcey + oy, source_pixel);
                }
            }
        }
    }
}

void SpriteBlitScaledSampledOrigin(Sprite* sprite, Sprite* target, float x, float y, float scale, float originx, float originy){
    if(scale == 0)return;
    
    int clip_left = x-originx*scale;
    int clip_right = x+(sprite->w-originx)*scale+1;
    int clip_top = y-originy*scale;
    int clip_bottom = y+(sprite->h-originy)*scale+1;
    
    for(int tx = min(clip_left, clip_right); tx < max(clip_right, clip_left); tx++){
        for(int ty = min(clip_top, clip_bottom); ty < max(clip_bottom, clip_top); ty++){
            SpritePutPixelAlphaBlended(target, tx, ty, SpriteGetPixel(sprite, (tx-clip_left)/scale, (ty-clip_top)/scale));
        }
    }
}
void SpriteBlitScaledSampled(Sprite* sprite, Sprite* target, float x, float y, float scale){
    SpriteBlitScaledSampledOrigin(sprite, target, x, y, scale, sprite->w/2, sprite->h/2);
}

void SpriteBlitScaledSampledXY(Sprite* sprite, Sprite* target, float x, float y, float scalex, float scaley, float originx, float originy){
    if(scalex == 0 || scaley == 0)return;
    
    int clip_left = x-originx*scalex;
    int clip_right = x+(sprite->w-originx)*scalex+1;
    int clip_top = y-originy*scaley;
    int clip_bottom = y+(sprite->h-originy)*scaley+1;
    
    for(int tx = min(clip_left, clip_right); tx < max(clip_right, clip_left); tx++){
        for(int ty = min(clip_top, clip_bottom); ty < max(clip_bottom, clip_top); ty++){
            SpritePutPixelAlphaBlended(target, tx, ty, SpriteGetPixel(sprite, (tx-clip_left)/scalex, (ty-clip_top)/scaley));
        }
    }
}

void SpriteBlitRotatedOrigin(Sprite* sprite, Sprite* target, int x, int y, float angle, int originx, int originy){
    // Rotating freely causes gaps
    //float s = sinf(angle);
    //float c = cosf(angle);
    
    // Restrict to 90 degree angles
    angle = ((int)(angle/(M_PI/2.f))%4) * (M_PI/2.f);
    int s = sinf(angle);
    int c = cosf(angle);
    
    for(int sy = 0; sy < sprite->h; sy++){
        for(int sx = 0; sx < sprite->w; sx++){
            int centerx = sx - originx + 0.5f;
            int centery = sy - originy + 0.5f;
            SpritePutPixelAlphaBlended(target, x + centerx*c - centery*s + originx, y + centerx*s + centery*c + originy, SpriteGetPixel(sprite, sx, sy));
        }
    }
}
void SpriteBlitRotated(Sprite* sprite, Sprite* target, int x, int y, float angle){
    SpriteBlitRotatedOrigin(sprite, target, x, y, angle, sprite->w/2, sprite->h/2);
}

void SpriteBlitRotatedSampledOrigin(Sprite* sprite, Sprite* target, int x, int y, float angle, float originx, float originy){
    float s = sinf(-angle);
    float c = cosf(-angle);
    
    float l = -originx;
    float r = sprite->w-originx;
    float t = -originy;
    float b = sprite->h-originy;
    
    float origins = sinf(angle);
    float originc = cosf(angle);
    
    float rotated_originx = originx*originc - originy*origins;
    float rotated_originy = originx*origins + originy*originc;
    
    int corners[] = {
        (int)(l*originc - t*origins + originx),
        (int)(l*origins + t*originc + originy),
        
        (int)(r*originc - t*origins + originx),
        (int)(r*origins + t*originc + originy),
        
        (int)(r*originc - b*origins + originx),
        (int)(r*origins + b*originc + originy),
        
        (int)(l*originc - b*origins + originx),
        (int)(l*origins + b*originc + originy)
    };
    
    int left = x+min(corners[0], min(corners[2], min(corners[4], corners[6])));
    int right = x+max(corners[0], max(corners[2], max(corners[4], corners[6])))+2;
    int top = y+min(corners[1], min(corners[3], min(corners[5], corners[7])));
    int bottom = y+max(corners[1], max(corners[3], max(corners[5], corners[7])))+2;
    
    int spriteox = -x - originx;
    int spriteoy = -y - originy;
    
    for(int tx = left; tx < right; tx++){
        for(int ty = top; ty < bottom; ty++){
            int spritex = tx+spriteox;
            int spritey = ty+spriteoy;
            SpritePutPixelAlphaBlended(target, tx, ty, SpriteGetPixel(
                sprite,
                spritex*c - spritey*s + originx,
                spritex*s + spritey*c + originy));
        }
    }
}
void SpriteBlitRotatedSampled(Sprite* sprite, Sprite* target, int x, int y, float angle){
    SpriteBlitRotatedSampledOrigin(sprite, target, x, y, angle, sprite->w/2, sprite->h/2);
}

void SpriteBlitScaledRotatedSampledOrigin(Sprite* sprite, Sprite* target, float x, float y, float scale, float angle, float originx, float originy){
    float s = sinf(-angle);
    float c = cosf(-angle);
    
    float l = -originx;
    float r = sprite->w-originx;
    float t = -originy;
    float b = sprite->h-originy;
    
    float origins = sinf(angle);
    float originc = cosf(angle);
    
    float rotated_originx = originx*originc - originy*origins;
    float rotated_originy = originx*origins + originy*originc;
    
    int corners[] = {
        (int)((l*originc - t*origins)*scale + originx),
        (int)((l*origins + t*originc)*scale + originy),
        
        (int)((r*originc - t*origins)*scale + originx),
        (int)((r*origins + t*originc)*scale + originy),
        
        (int)((r*originc - b*origins)*scale + originx),
        (int)((r*origins + b*originc)*scale + originy),
        
        (int)((l*originc - b*origins)*scale + originx),
        (int)((l*origins + b*originc)*scale + originy)
    };
    
    int left = x+min(corners[0], min(corners[2], min(corners[4], corners[6])));
    int right = x+max(corners[0], max(corners[2], max(corners[4], corners[6])));
    int top = y+min(corners[1], min(corners[3], min(corners[5], corners[7])));
    int bottom = y+max(corners[1], max(corners[3], max(corners[5], corners[7])));
    
    float spriteox = -x - originx;
    float spriteoy = -y - originy;
    for(int tx = left; tx < right; tx++){
        for(int ty = top; ty < bottom; ty++){
            float spritex = tx + spriteox;
            float spritey = ty + spriteoy;
            SpritePutPixelAlphaBlended(target, tx, ty, SpriteGetPixel(
                sprite,
                (spritex*c - spritey*s)/scale + originx,
                (spritex*s + spritey*c)/scale + originy));
        }
    }
}
void SpriteBlitScaledRotatedSampled(Sprite* sprite, Sprite* target, int x, int y, float scale, float angle){
    SpriteBlitScaledRotatedSampledOrigin(sprite, target, x, y, scale, angle, sprite->w/2, sprite->h/2);
}

void SpriteBlitScaledRotatedSampledXY(Sprite* sprite, Sprite* target, float x, float y, float scalex, float scaley, float angle, float originx, float originy){
    float s = sinf(-angle);
    float c = cosf(-angle);
    
    float l = -originx;
    float r = sprite->w-originx;
    float t = -originy;
    float b = sprite->h-originy;
    
    float origins = sinf(angle);
    float originc = cosf(angle);
    
    float rotated_originx = originx*originc - originy*origins;
    float rotated_originy = originx*origins + originy*originc;
    
    int corners[] = {
        (int)((l*originc*scalex - t*origins*scaley) + originx),
        (int)((l*origins*scalex + t*originc*scaley) + originy),
        
        (int)((r*originc*scalex - t*origins*scaley) + originx),
        (int)((r*origins*scalex + t*originc*scaley) + originy),
        
        (int)((r*originc*scalex - b*origins*scaley) + originx),
        (int)((r*origins*scalex + b*originc*scaley) + originy),
        
        (int)((l*originc*scalex - b*origins*scaley) + originx),
        (int)((l*origins*scalex + b*originc*scaley) + originy)
    };
    
    int left = x+min(corners[0], min(corners[2], min(corners[4], corners[6])));
    int right = x+max(corners[0], max(corners[2], max(corners[4], corners[6])));
    int top = y+min(corners[1], min(corners[3], min(corners[5], corners[7])));
    int bottom = y+max(corners[1], max(corners[3], max(corners[5], corners[7])));
    
    float spriteox = -x - originx;
    float spriteoy = -y - originy;
    for(int ty = top; ty < bottom; ty++){
        for(int tx = left; tx < right; tx++){
            float spritex = tx + spriteox;
            float spritey = ty + spriteoy;
            SpritePutPixelAlphaBlended(target, tx, ty, SpriteGetPixel(
                sprite,
                (spritex*c - spritey*s)/scalex + originx,
                (spritex*s + spritey*c)/scaley + originy));
        }
    }
}

Sprite framebuffer;

int main(int argc, char* argv[]){
    KWInit(1280, 720, (char*)"Nick's Sprite Blitting Window");
    //KWSetTargetFramerate(0);
    Sprite framebuffer;
    framebuffer.w = kw_framebuffer.w;
    framebuffer.h = kw_framebuffer.h;
    framebuffer.pixels = kw_framebuffer.pixels;
    
    Sprite kero_sprite;
    if(!SpriteLoad(&kero_sprite, (char*)"croakingkero.png")){
        return 0;
    }
    
    // Main loop
    int x = 128, y = 72;
    float sprite_rotation = 0.f;
    float sprite_scale = -0.f;
    game_running = true;
    while(game_running){
        
        // Handle events
        while(KWEventsQueued()){
            kwEvent* e = KWNextEvent();
            switch(e->type){
                case KWEVENT_KEY_PRESS:{
                    switch(e->key){
                        case KEY_ESCAPE:{
                            game_running = false;
                        }break;
                    }
                }break;
                case KWEVENT_RESIZE:{
                    framebuffer.w = kw_framebuffer.w;
                    framebuffer.h = kw_framebuffer.h;
                    framebuffer.pixels = kw_framebuffer.pixels;
                }break;
                case KWEVENT_QUIT:{
                    game_running = false;
                }break;
            }
            KWFreeEvent(e);
        }
        
        // Clear frame buffer
        memset(framebuffer.pixels, 0xff, framebuffer.w*framebuffer.h*4);
        
        // Move our pixel
        if(kw_keyboard[KEY_UP]){
            --y;
        }
        if(kw_keyboard[KEY_DOWN]){
            ++y;
        }
        if(kw_keyboard[KEY_LEFT]){
            --x;
        }
        if(kw_keyboard[KEY_RIGHT]){
            ++x;
        }
        
        if(true || kw_keyboard[KEY_SPACE]){
            sprite_rotation += 0.025f;
            sprite_scale += 0.05f;
        }
        
        // Draw our sprites
        SpriteBlit(&kero_sprite, &framebuffer, x, y);
        SpriteBlitRotatedOrigin(&kero_sprite, &framebuffer, x, y + 64, sprite_rotation, 32, 64);
        SpriteBlitRotatedSampledOrigin(&kero_sprite, &framebuffer, x + 184, y + 64, sprite_rotation, 0, 0);
        SpriteBlitScaledXY(&kero_sprite, &framebuffer, x + 384, y + 128, sinf(sprite_scale)*4.f, cosf(sprite_scale)*4.f, 32, 0);
        SpriteBlitScaledSampledXY(&kero_sprite, &framebuffer, x + 128, y + 384, sinf(sprite_scale)*4.f, cosf(sprite_scale)*4.f, 0, 0);
        SpriteBlitScaledRotatedSampledXY(&kero_sprite, &framebuffer, x + 384.f, y + 384.f, sinf(sprite_scale)*4.f, 1, sprite_rotation, 32.f, 64.f);
        
        KWFlipWindow();
    }
    
    return 0;
}

