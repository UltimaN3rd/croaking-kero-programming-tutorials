#include "kero_window.h"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <stb/stb_image.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#define Min(a,b) (a<b ? a:b)
#define Max(a,b) (a>b ? a:b)
//#define Sign(a) (a<0 ? -1:1)

typedef struct{
    int w, h;
    uint32_t* pixels;
} Sprite;

bool SpriteLoad(char* file, Sprite* sprite){
    sprite->pixels = (uint32_t*)stbi_load(file, &sprite->w, &sprite->h, 0, 0);
    if(!sprite->pixels){
        return false;
    }
    
    for(int y = 0; y < sprite->h; y++){
        for(int x = 0; x < sprite->w; x++){
            uint8_t* b = (uint8_t*)(sprite->pixels + y*sprite->w + x);
            uint8_t* r = b + 2;
            uint8_t true_red = *b;
            *b = *r;
            *r = true_red;
        }
    }
    
    return true;
}

uint32_t SpriteGetPixel(Sprite* source, int x, int y){
    if(x < 0 || x > source->w-1 || y < 0 || y > source->h-1)return 0;
    return source->pixels[y*source->w + x];
}

void SpriteSetPixel(Sprite* dest, int x, int y, uint32_t pixel){
    if(x < 0 || x > dest->w-1 || y < 0 || y > dest->h-1)return;
    dest->pixels[y*dest->w + x] = pixel;
}

void SpriteBlendPixel(Sprite* dest, int x, int y, uint32_t pixel){
    if(x < 0 || x > dest->w-1 || y < 0 || y > dest->h-1)return;
    uint8_t* dest_pixel = (uint8_t*)&dest->pixels[y*dest->w + x];
    float alpha = (float)(pixel >> 24) / 255.f;
    float one_minus_alpha = 1.f - alpha;
    dest_pixel[0] = (uint8_t)(pixel) * alpha + dest_pixel[0] * one_minus_alpha;
    dest_pixel[1] = (uint8_t)(pixel>>8) * alpha + dest_pixel[1] * one_minus_alpha;
    dest_pixel[2] = (uint8_t)(pixel>>16) * alpha + dest_pixel[2] * one_minus_alpha;
    dest_pixel[3] += (255 - dest_pixel[3]) * alpha;
}

void SpriteBlit(Sprite* source, Sprite* dest, int x, int y, int originx, int originy){
    int left = x - originx;
    int top = y - originy;
    
    int left_clip = Max(0, -left);
    int right_clip = Max(0, left + source->w - dest->w);
    int top_clip = Max(0, -top);
    int bottom_clip = Max(0, top + source->h - dest->h);
    
    for(int sourcey = top_clip; sourcey < source->h - bottom_clip; sourcey++){
        for(int sourcex = left_clip; sourcex < source->w - right_clip; sourcex++){
            SpriteBlendPixel(dest, left+sourcex, top+sourcey, source->pixels[sourcey*source->w + sourcex]);
        }
    }
}

void SpriteBlitScaled(Sprite* source, Sprite* dest, float x, float y, float scalex, float scaley, float originx, float originy){
    /*int left_clip = Max(0, -x);
    int right_clip = Max(0, x + source->w*scalex - dest->w);
    int top_clip = Max(0, -y);
    int bottom_clip = Max(0, y + source->h*scaley - dest->h);*/
    
    for(int sourcey = 0; sourcey < source->h; sourcey++){
        for(int sourcex = 0; sourcex < source->w; sourcex++){
            uint32_t source_pixel = source->pixels[sourcey*source->w + sourcex];
            int destx = x + (sourcex - originx) * scalex;
            int desty = y + (sourcey - originy) * scaley;
            for(int dy = Min(0, scaley); dy < Max(scaley, 1); dy++){
                for(int dx = Min(0, scalex); dx < Max(scalex, 1); dx++){
                    SpriteBlendPixel(dest, destx + dx, desty + dy, source_pixel);
                }
            }
        }
    }
}

void SpriteBlitRotated(Sprite* sprite, Sprite* target, float x, float y, float angle, float originx, float originy){
    float s = sinf(angle);
    float c = cosf(angle);
    
    for(int sourcey = 0; sourcey < sprite->h; ++sourcey){
        for(int sourcex = 0; sourcex < sprite->w; ++sourcex){
            float translatedx = sourcex - originx;
            float translatedy = sourcey - originy;
            float destx = x+originx + translatedx*c - translatedy*s;
            float desty = y+originy + translatedx*s + translatedy*c;
            SpriteBlendPixel(target, destx, desty, SpriteGetPixel(sprite, sourcex, sourcey));
        }
    }
}

void SpriteBlitScaledSampled(Sprite* source, Sprite* dest, float x, float y, float scalex, float scaley, float originx, float originy){
    int left = Max(x, 0);
    int right = Min(x + source->w*scalex, dest->w-1);
    int top = Max(y, 0);
    int bottom = Min(y + source->h*scaley, dest->h-1);
    
    for(int dy = top; dy < bottom; dy++){
        for(int dx = left; dx < right; dx++){
            SpriteBlendPixel(dest, dx, dy, SpriteGetPixel(source, (dx - left)/scalex, (dy - top)/scaley));
        }
    }
}

int main(int argc, char* argv[]){
    KWInit(1280, 720, (char*)"Nick's Sprite Blitting Window");
    
    Sprite kero_sprite;
    if(!SpriteLoad((char*)"croakingkero.png", &kero_sprite)){
        printf("Failed to load sprite.\n");
        return 0;
    }
    
    Sprite framebuffer;
    framebuffer.pixels = (uint32_t*)kw_framebuffer.pixels;
    framebuffer.w = kw_framebuffer.w;
    framebuffer.h = kw_framebuffer.h;
    
    // Main loop
    bool game_running = true;
    int x = 0, y = 0;
    float scalex = 1.f, scaley = 1.f;
    float rot = 0.f;
    while(game_running){
        while(KWEventsQueued()){
            kwEvent* e = KWNextEvent();
            switch(e->type){
                case KWEVENT_KEY_PRESS:{
                    switch(e->key){
                        case KEY_ESCAPE:{
                            game_running = false;
                        }break;
                    }
                }break;
                case KWEVENT_QUIT:{
                    game_running = false;
                }break;
                case KWEVENT_RESIZE:{
                    framebuffer.w = kw_framebuffer.w;
                    framebuffer.h = kw_framebuffer.h;
                    framebuffer.pixels = (uint32_t*)kw_framebuffer.pixels;
                }break;
            }
            KWFreeEvent(e);
        }
        
        if(kw_keyboard[KEY_UP]){
            --y;
        }
        if(kw_keyboard[KEY_DOWN]){
            ++y;
        }
        if(kw_keyboard[KEY_LEFT]){
            --x;
        }
        if(kw_keyboard[KEY_RIGHT]){
            ++x;
        }
        
        if(kw_keyboard[KEY_X]){
            scalex += 0.05f;
        }
        if(kw_keyboard[KEY_Y]){
            scaley += 0.05f;
        }
        rot += 0.01f;
        
        // Clear frame buffer
        memset(kw_framebuffer.pixels, 0xff, kw_framebuffer.w * kw_framebuffer.h * 4);
        
        //SpriteBlit(&kero_sprite, &framebuffer, x, y, 0, 0);
        //SpriteBlitScaled(&kero_sprite, &framebuffer, x, y, sin(scalex)*10.f, sin(scaley)*10.f, 32, 32);
        //SpriteBlitScaledSampled(&kero_sprite, &framebuffer, x, y, sin(scalex)*10.f, sin(scaley)*10.f);
        SpriteBlitRotated(&kero_sprite, &framebuffer, x, y, rot, 32, 32);
        
        KWFlipWindow();
    }
}